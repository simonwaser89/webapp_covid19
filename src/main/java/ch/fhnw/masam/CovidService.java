package ch.fhnw.masam;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.event.NamedEvent;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

@NamedEvent
@ApplicationScoped
@ManagedBean(name = "covidService")
public class CovidService {

	private String country;
	private List<DataRow> data = new ArrayList<>();
	private List<String> countries = null;
	private String hostname = "ec2-54-234-120-115.compute-1.amazonaws.com:8080";
	
	private final OkHttpClient httpClient;
	
	public CovidService() {
		httpClient = new OkHttpClient.Builder()
			      .connectTimeout(30, TimeUnit.SECONDS)
			      .readTimeout(30, TimeUnit.SECONDS)
			      .writeTimeout(30, TimeUnit.SECONDS)
			      .build();
	}
	
	public List<String> getCountries() {
		if (countries == null) {
			Request request = new Request.Builder().url("http://" + hostname + "/covidservice/allcountries").build();
			try (Response response = httpClient.newCall(request).execute()) {
				Gson g = new Gson();
				Type stringType = new TypeToken<List<String>>() {}.getType();
				countries = g.fromJson(response.body().string(), stringType);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return countries;
	}
	
	public String getCountry() {
		return country;
	}
	
	public void setCountry(String country) {
		this.country = country;
	}
	
	public void retrieveData() {
		System.out.println("retrieve data");
		data.clear();
		// retrieve data from the service
		Request request = new Request.Builder().url("http://" + hostname + "/covidservice/bycountry?country=" + country).build();
		try (Response response = httpClient.newCall(request).execute()) {
			Gson g = new Gson();
			Type dataRowType = new TypeToken<List<DataRow>>() {}.getType();
			data = g.fromJson(response.body().string(), dataRowType);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	
	public List<DataRow> getDataRows() {
		return data;
	}
}
