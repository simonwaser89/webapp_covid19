package ch.fhnw.masam;

import java.util.concurrent.TimeUnit;

import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SimpleSeleniumTest {

	@Test
	public void webTest() {
		System.setProperty("webdriver.chrome.driver", "D:\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		
		String url = "http://localhost:8090/masam_covid_app/";
		driver.get(url);
		
		String pageTitle = driver.getTitle();
		System.out.println(pageTitle);
		
		WebElement cDropdown = driver.findElement(By.id("inputform:city_input"));
		WebElement retrieveButton = driver.findElement(By.id("inputform:retrieveButton"));
		Select select = new Select(cDropdown);
		
		select.selectByValue("Switzerland");
		retrieveButton.click();
		
		System.out.println(cDropdown);
		System.out.println(retrieveButton);
		
		driver.close();
	}
	
}
